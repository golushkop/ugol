import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

const appRoutes: Routes = [
  {
    path: '',
    loadChildren: './modules/default/default.module#DefaultModule'
  },
  {
    path: 'filter',
    loadChildren: './modules/filter/filter.module#FilterModule'
  },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
