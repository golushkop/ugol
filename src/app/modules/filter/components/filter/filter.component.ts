import { Component, OnInit } from '@angular/core';
import {GetDataService} from '../../../shared/services/get-data.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  public load: boolean;
  private items : Array<Object>;
  public filters: Array<Object>;
  public items_to_show: Array<Object>;
  private filter_chosen = {
    brand: '',
    color: '',
    format: ''
  };
  constructor(private _get_data: GetDataService) { }

  ngOnInit() {
    this.load = true;
    this.getData()
  }

  private getData() {
    this._get_data.get_main_data()
      .subscribe(
        (res: any) => {
          this.items = res['items'];
          this.filters = res['filters'];
          this.filterItems();
          this.load = false
        }
      )
  }

  private setFilter(filter_value = null, filter_option = null) {
    this.filters.forEach(filter=>{
      if (filter_value && filter['value'] === filter_value) {
        filter['selected'] = !!filter_option;
        filter['params'].forEach(param => {
          param['selected'] = filter_option && param['name'] === filter_option
        })
      } else if(!filter_value) {
        filter['selected'] = false;
        filter['params'].forEach(param => {
          param['selected'] = false
        })
      }
    })
  }

  public filterItems(filter_value = null, filter_option = null) {
    this.setFilter(filter_value, filter_option);
    if (filter_value) {
      this.filter_chosen[filter_value] = filter_option;
      this.items_to_show = this.items.filter(item => {
        let result = true;
        for (let i = 0; i < item['params'].length; i++) {
          const param = item['params'][i];
          if (this.filter_chosen[param['param']] && this.filter_chosen[param['param']] !== param['value']) {
            result = false;
            break
          }
        }
        return result
      })
    } else {
      this.items_to_show = this.items
    }
  }

}
