import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './components/filter/filter.component';
import {RouterModule} from '@angular/router';
import {FilterRoutes} from './filter.module.routes';
import {SharedModule} from '../shared/shared.module';
import {MaterialModule} from '../material/material.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(FilterRoutes),
    MaterialModule,
    SharedModule
  ],
  declarations: [FilterComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FilterModule { }
