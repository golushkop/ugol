import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {GetDataService} from './services/get-data.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
     return {
       ngModule: SharedModule,
       providers: [
          GetDataService
       ]
     }
   }
}
