import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class GetDataService {

  constructor(private _http: HttpClient) { }

  public get_main_data(): Observable<any> {
    return this._http.get('data/catalog.json')
  }

}
