import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './components/default/default.component';
import {RouterModule} from '@angular/router';
import {DefaultRoutes} from './default.module.routes';
import {MaterialModule} from '../material/material.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DefaultRoutes),
    MaterialModule,
  ],
  declarations: [DefaultComponent]
})
export class DefaultModule { }
