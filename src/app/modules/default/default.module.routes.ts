import {DefaultComponent} from './components/default/default.component';

export const DefaultRoutes = [
  {
    path: '',
    component: DefaultComponent,
}];
